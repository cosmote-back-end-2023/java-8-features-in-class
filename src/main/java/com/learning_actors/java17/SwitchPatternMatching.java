/*package com.learning_actors.java17;

public class SwitchPatternMatching {

    *//*
        NOTE that since this is a Java Preview Feature, you might need to either enable preview,
        or have JDK 21 available in order to properly compile below
     *//*
    public static void main(String[] args) {
        Rectangle r = new Rectangle(33.33, 25.23);
        Circle c = new Circle(6.25);

        System.out.println(checkSize(r));
        System.out.println(checkSize(c));

    }

    public static String checkSize(Shape shape) {
        return switch (shape) {
            case Rectangle r when (Double.parseDouble(getCircumReference(r)) > 100) -> "This is a big rectangle";
            case Circle c when (Double.parseDouble(getCircumReference(c)) > 100) -> "This is a big circle";
            default -> "Normal sized shape";
        };
    }

    public static String checkSizeFullCoverageNoDefaultBranchNeeded(Shape shape) {
        return switch (shape) {
            case Rectangle r when (Double.parseDouble(getCircumReference(r)) > 100) -> "This is a big rectangle";
            case Rectangle r -> "This is a normal sized rectangle";
            case Circle c when (Double.parseDouble(getCircumReference(c)) > 100) -> "This is a big circle";
            case Circle c -> "This is a normal sized cycle";
            //the default branch isn't needed
            //default -> "Normal sized shape";
        };
    }

    public static String getCircumReference(Object obj) {
        return switch (obj) {
            case Rectangle r -> String.valueOf(2 * r.getLength() + 2 * r.getWidth());
            case Circle c -> String.valueOf(2 * c.getRadius() * Math.PI);
            case null -> "It is null";
            default -> throw new IllegalArgumentException("Unknown shape");
        };
    }

    //the following won't compile if we remove the default clause, due to all selector types not being covered
    static double getDoubleUsingSwitch(Object o) {
        return switch (o) {
            case String s -> Double.parseDouble(s);
            default -> 0.0;
        };
    }

}*/
