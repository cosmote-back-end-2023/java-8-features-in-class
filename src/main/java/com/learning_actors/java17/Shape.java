package com.learning_actors.java17;

sealed interface Shape permits Circle, Rectangle {
}
