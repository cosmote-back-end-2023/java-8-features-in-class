package com.learning_actors.java8.lambdas.comparator_step1;

import java.util.Comparator;

import com.learning_actors.java8.common.PhoneBill;

class PhoneBillComparator implements Comparator<PhoneBill> {

    @Override
    public int compare(PhoneBill o1, PhoneBill o2) {
        return o1.getCost().compareTo(o2.getCost());
    }

}
