package com.learning_actors.java8.lambdas.comparator_step1;

import java.util.List;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;
import com.learning_actors.java8.common.TestDataProvider;

public class ComparatorClassExample {

    private static final TestDataProvider TEST_DATA_PROVIDER = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {
        Customer customer = TEST_DATA_PROVIDER.provideCustomerWithOutAddress();
        List<PhoneBill> customerBills = customer.getPhoneBills();

        PhoneBillComparator phoneBillComparator = new PhoneBillComparator();
        customerBills.sort(phoneBillComparator);
        //customerBills.sort(phoneBillComparator.reversed());
        System.out.println(customerBills);
    }

}
