package com.learning_actors.java8.lambdas.comparator_step3;

import java.util.List;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;
import com.learning_actors.java8.common.TestDataProvider;


public class ComparatorLambdaExample {

    private static final TestDataProvider TEST_DATA_PROVIDER = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {
        Customer customer = TEST_DATA_PROVIDER.provideCustomerWithOutAddress();
        List<PhoneBill> customerBills = customer.getPhoneBills();

        customerBills.sort((PhoneBill pb1, PhoneBill pb2) -> pb2.getCost().compareTo(pb1.getCost()));

        System.out.println(customerBills);
    }

}
