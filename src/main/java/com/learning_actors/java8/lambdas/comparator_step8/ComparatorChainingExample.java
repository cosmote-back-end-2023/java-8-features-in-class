package com.learning_actors.java8.lambdas.comparator_step8;

import static java.util.Comparator.comparing;

import java.util.Comparator;
import java.util.List;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;
import com.learning_actors.java8.common.TestDataProvider;

public class ComparatorChainingExample {

    private static final TestDataProvider TEST_DATA_PROVIDER = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {
        Customer customer = TEST_DATA_PROVIDER.provideCustomerWithOutAddress();
        List<PhoneBill> customerBills = customer.getPhoneBills();

        Comparator<PhoneBill> phoneBillComparatorByCost = comparing(PhoneBill::getCost);
        Comparator<PhoneBill> phoneBillComparatorByPhoneNumber = comparing(PhoneBill::getPhoneNumber);

        customerBills.sort(phoneBillComparatorByCost
            .reversed()
            .thenComparing(phoneBillComparatorByPhoneNumber));

        System.out.println(customerBills);
    }

}
