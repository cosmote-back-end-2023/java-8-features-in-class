package com.learning_actors.java8.lambdas.comparator_step7;

import java.util.Comparator;
import java.util.List;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;
import com.learning_actors.java8.common.TestDataProvider;


public class ComparatorMethodReferenceExample {

    private static final TestDataProvider TEST_DATA_PROVIDER = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {
        Customer customer = TEST_DATA_PROVIDER.provideCustomerWithOutAddress();
        List<PhoneBill> customerBills = customer.getPhoneBills();

        customerBills.sort(Comparator.comparing(PhoneBill::getCost));
        System.out.println(customerBills);
    }

}
