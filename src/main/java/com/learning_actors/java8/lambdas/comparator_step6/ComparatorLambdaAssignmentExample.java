package com.learning_actors.java8.lambdas.comparator_step6;

import java.util.Comparator;
import java.util.List;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;
import com.learning_actors.java8.common.TestDataProvider;


public class ComparatorLambdaAssignmentExample {

    private static final TestDataProvider TEST_DATA_PROVIDER = TestDataProvider.getTestDataProviderInstance();

    private static Comparator<PhoneBill> phoneBillComparatorByCost = Comparator.comparing(pb -> pb.getCost());
    private static Comparator<PhoneBill> phoneBillComparatorByPhoneNumber = Comparator.comparing(pb -> pb.getPhoneNumber());

    public static void main(String[] args) {
        Customer customer = TEST_DATA_PROVIDER.provideCustomerWithOutAddress();
        List<PhoneBill> customerBills = customer.getPhoneBills();

        customerBills.sort(phoneBillComparatorByCost);
        System.out.println(customerBills);

        customerBills.sort(phoneBillComparatorByPhoneNumber.reversed());
        System.out.println(customerBills);
    }

}
