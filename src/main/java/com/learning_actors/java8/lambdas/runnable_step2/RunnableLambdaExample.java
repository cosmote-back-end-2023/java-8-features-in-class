package com.learning_actors.java8.lambdas.runnable_step2;

import java.util.List;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;
import com.learning_actors.java8.common.TestDataProvider;

public class RunnableLambdaExample {

    private static final TestDataProvider TEST_DATA_PROVIDER = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {
        Customer customer = TEST_DATA_PROVIDER.provideCustomerWithOutAddress();
        Customer customerWithAddressAndNoStreet = TEST_DATA_PROVIDER.provideCustomerWithAddressAndNoStreet();
        Customer customerWithAddressAndStreet = TEST_DATA_PROVIDER.provideCustomerWithAddressAndStreet();

        List<Customer> customers = List.of(customer, customerWithAddressAndStreet, customerWithAddressAndNoStreet);


        //this example is meant to be improved through collection streams and lambdas
        for (Customer c : customers) {
            String threadName = c.getName();
            Runnable task = getRunnable(c, threadName);
            task.run();
        }

    }

    private static Runnable getRunnable(Customer c, String threadName) {
        Runnable task = () -> {
            System.out.println("Printing bills for customer: " + threadName);
            for (PhoneBill pb: c.getPhoneBills()) {
                System.out.println(pb);
            }
        };
        return task;
    }

}
