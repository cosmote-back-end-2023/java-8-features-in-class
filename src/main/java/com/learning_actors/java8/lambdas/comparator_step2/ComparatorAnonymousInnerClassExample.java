package com.learning_actors.java8.lambdas.comparator_step2;

import java.util.Comparator;
import java.util.List;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;
import com.learning_actors.java8.common.TestDataProvider;


public class ComparatorAnonymousInnerClassExample {

    private static final TestDataProvider TEST_DATA_PROVIDER = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {
        Customer customer = TEST_DATA_PROVIDER.provideCustomerWithOutAddress();
        List<PhoneBill> customerBills = customer.getPhoneBills();

        customerBills.sort(new Comparator<PhoneBill>() {
            @Override
            public int compare(PhoneBill o1, PhoneBill o2) {
                return o1.getPhoneNumber().compareTo(o2.getPhoneNumber());
            }
        });

        System.out.println(customerBills);
    }

}
