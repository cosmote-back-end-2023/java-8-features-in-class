package com.learning_actors.java8.functional_interfaces;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.TestDataProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PredicateDemo {

    private static final TestDataProvider TEST_DATA_PROVIDER = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {
        Customer customerWithAddressAndNoStreet = TEST_DATA_PROVIDER.provideCustomerWithAddressAndNoStreet();
        Customer customerWithAddressAndStreet = TEST_DATA_PROVIDER.provideCustomerWithAddressAndStreet();

        List<Customer> customers = List.of(customerWithAddressAndStreet, customerWithAddressAndNoStreet);

        System.out.println(filterCustomer(customers, c -> c.getName().startsWith("C")));
        System.out.println(filterCustomer(customers, c -> "Athens".equals(c.getAddress().getCity())));
    }

    private static List<Customer> filterCustomer(List<Customer> customersToFilter, Predicate<Customer> customerPredicate) {
        List<Customer> filteredCustomerList = new ArrayList<>();
        for (Customer customer: customersToFilter) {
            if (customerPredicate.test(customer)) {
                filteredCustomerList.add(customer);
            }
        }
        return filteredCustomerList;
    }



}
