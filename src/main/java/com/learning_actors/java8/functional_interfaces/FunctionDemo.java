package com.learning_actors.java8.functional_interfaces;

import java.util.function.Function;

public class FunctionDemo {

    public static void main(String[] args) {
        int incr = 20;
        int myNumber = 10;
        modifyTheValue(myNumber, val-> val + incr);

        myNumber = 15;
        modifyTheValue(myNumber, val-> val * 10);
        modifyTheValue(myNumber, val-> val - 100);
        modifyTheValue(myNumber, val-> "somestring".length() + val - 100);

    }


    private static void modifyTheValue(int valueToModify, Function<Integer, Integer> functionToApply) {
        int newValue = functionToApply.apply(valueToModify);
        /*
         * Do some operations using the new value.
         */
        System.out.println(newValue);
    }

}
