package com.learning_actors.java8.optional;

import java.util.Optional;

import com.learning_actors.java8.common.Address;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.Street;

public class CustomerAddressServicePrivateMethodsImpl implements CustomerAddressService {


    @Override
    public Address getCustomerAddress(Customer customer) {
        return Optional.ofNullable(customer.getAddress()).orElse(new Address("default PC", "default City", "default State", null));
    }

    public Optional<Address> getCustomerAddressOptional(Customer customer) {
        Optional<Address> addressOptional = Optional.ofNullable(customer.getAddress());
        System.out.println("Customer address is present: " + addressOptional.isPresent());
        System.out.println("Customer address is empty : " + addressOptional.isEmpty());
        return addressOptional;
    }

    @Override
    public String getCustomerPostalCode(Customer customer) {
        return Optional.of(customer)
            .map(c -> c.getAddress())
            .map(a -> a.getPostalCode())
            .orElse("No postal code registered for this customer");
    }

    @Override
    public String getCustomerStreetName(Customer customer) {
        return getStreet(customer)
            .map(Street::getStreetName)
            .orElse("No street name found for this customer");
    }

    public String getCustomerState(Customer customer) {
        return Optional.of(customer)
            .map(Customer::getAddress)
            .map(Address::getState)
            .orElse("No state found for this customer");
    }

    public String getCustomerAppartmentLetter(Customer customer) {
        return getStreet(customer)
            .flatMap(Street::getApartmentLetter)
            .orElse("No appartment letter for this address");
    }

    public Optional<String> getOptionalCustomerAppartmentLetter(Customer customer) {
        return getStreet(customer)
            .flatMap(Street::getApartmentLetter);
    }

    private Optional<Street> getStreet(Customer customer) {
        return Optional.of(customer)
            .map(Customer::getAddress)
            .map(Address::getStreet);
    }


}
