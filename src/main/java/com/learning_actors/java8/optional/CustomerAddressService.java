package com.learning_actors.java8.optional;

import com.learning_actors.java8.common.Address;
import com.learning_actors.java8.common.Customer;

public interface CustomerAddressService {

    Address getCustomerAddress(Customer customer);

    String getCustomerPostalCode(Customer customer);

    String getCustomerStreetName(Customer customer);

}
