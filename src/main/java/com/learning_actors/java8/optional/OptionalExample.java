package com.learning_actors.java8.optional;

import com.learning_actors.java8.common.Address;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.TestDataProvider;

import java.util.NoSuchElementException;
import java.util.Optional;

public class OptionalExample {

    private static final CustomerAddressService customerAddressService = new CustomerAddressServiceImpl();
    private static final CustomerAddressServiceOptionalImpl customerAddressServiceOptional = new CustomerAddressServiceOptionalImpl();
    private static final TestDataProvider testDataProvider = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {

        Customer customerWithAddressAndStreet = testDataProvider.provideCustomerWithAddressAndStreet();
        Customer customerWithOutAddress = testDataProvider.provideCustomerWithOutAddress();
        Customer customerWithAddressAndNoStreet = testDataProvider.provideCustomerWithAddressAndNoStreet();

        Optional<Address> resultAddress = null;


        /*
            ***BASIC OPTIONAL OPERATIONS****
         */

        //attempting to get an address for a customer containing one, no issue raised
        System.out.println(customerAddressService.getCustomerAddress(customerWithAddressAndNoStreet));

        //attempting to get an address from a customer not containing one
        System.out.println(customerAddressService.getCustomerAddress(customerWithOutAddress));

        //after slide 44 create optional service and also getCustomerAddress with Optional
        //attempting to get an address from a customer not containing one, from an optional handling empty values
        System.out.println(customerAddressServiceOptional.getCustomerAddress(customerWithOutAddress));

        //after slide 45 create getCustomerAddressOptional
        //attempting to get an address from a customer not containing one, from an optional returning method
        resultAddress = customerAddressServiceOptional.getCustomerAddressOptional(customerWithOutAddress);
        System.out.println(resultAddress);

        //after slide 47 call .get() without if first
        //calling a get() after checking if isPresent() (not the intended usage of Optional)
        //calling .get() without checking whether a value is present will result in NoSuchElementException
        if (resultAddress.isPresent()) {
            resultAddress.get();
        }


        //attempting to get an address from a customer containing one, from an optional returning method
        resultAddress = customerAddressServiceOptional.getCustomerAddressOptional(customerWithAddressAndStreet);
        System.out.println(resultAddress);

        //after slide 48
        //do something when address is present
        resultAddress.ifPresent(r -> System.out.println(r));
        resultAddress.ifPresent(System.out::println);

        //after slide 49
        //filter values
        resultAddress
            .filter(address -> "Athens".equals(address.getCity()))
            .ifPresent(address -> System.out.println("This customer is based in Athens"));


        //Java 9 addition ifPresentOrElse() method
        resultAddress
            .filter(address -> "Sparta".equals(address.getCity()))
            .ifPresentOrElse(address -> System.out.println("This customer is based in Sparta"),
                () -> System.out.println("This customer isn't based in Sparta"));



        /*
         *** ACTUAL OPTIONAL USAGE BENEFITS, refer to the service implementations to compare****
         */

        //after slide 55
        //attempt to get postal code from a null address, using no null-checks, traditional way, null pointer exception
        //customerAddressService.getCustomerPostalCode(customerWithOutAddress);

        //attempt to get postal code from a null address, implementation using Optional map()
        System.out.println(customerAddressServiceOptional.getCustomerPostalCode(customerWithOutAddress));

        //attempt to get street from a null address, using tradition null-checks, traditional way, no issue but verbose implementation
        System.out.println(customerAddressService.getCustomerStreetName(customerWithOutAddress));

        //attempt to get postal code from a null address, implementation using Optional map(). more concise
        System.out.println(customerAddressServiceOptional.getCustomerStreetName(customerWithOutAddress));

        System.out.println();


        //after slide 57
        //get an optional field in Street object, API signature without any indication of empty value possibility, returning a warning message
        System.out.println(customerAddressServiceOptional.getCustomerAppartmentLetter(customerWithOutAddress));

        //get an optional field in Street object, API signature indicates optionality, handling is in client's discretion
        System.out.println(customerAddressServiceOptional.getOptionalCustomerAppartmentLetter(customerWithOutAddress));

        //get an optional field in Street object, API signature indicates optionality, using Java9 Optional.or() method
        System.out.println(customerAddressServiceOptional.getOptionalCustomerAppartmentLetterUsingJava9Or(customerWithOutAddress));


        //after slide 58
        //get address from a method that uses Java10 orElseThrowMethod() will throw NoSuchElementException of no value is present
        try {
            System.out.println(customerAddressServiceOptional.getCustomerAddressJava9OrElseThrow(customerWithOutAddress));
        } catch (NoSuchElementException e) {
            System.out.println("NoSuchElementException was thrown");
        }



    }

}
