package com.learning_actors.java8.optional;

import com.learning_actors.java8.common.Address;
import com.learning_actors.java8.common.Customer;

public class CustomerAddressServiceImpl implements CustomerAddressService {

    @Override
    public Address getCustomerAddress(Customer customer) {
        return customer.getAddress();
    }

    @Override
    public String getCustomerPostalCode(Customer customer) {
        // we don't have null checks intentionally, to produce a null pointer exception
        return customer.getAddress().getPostalCode();
    }

    @Override
    public String getCustomerStreetName(Customer customer) {
        if (customer.getAddress() != null) {
            Address address = customer.getAddress();
            if (address.getStreet() != null) {
                return address.getStreet().getStreetName();
            }
        }
        return "No street name found for this customer";
    }


}
