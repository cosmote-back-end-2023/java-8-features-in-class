package com.learning_actors.java8.common;

import java.math.BigDecimal;
import java.util.Arrays;

public class TestDataProvider {

    private static final TestDataProvider TEST_DATA_PROVIDER_INSTANCE = new TestDataProvider();

    private TestDataProvider() {
    }

    public static TestDataProvider getTestDataProviderInstance() {
        return TEST_DATA_PROVIDER_INSTANCE;
    }

    Street street = new Street("Alexandras", "16");

    Address address = new Address("11525", "Athens", "Attica", street);
    Address addressWithNoStreet = new Address("11525", "Athens", "Attica", null);

    private final Customer customerWithoutAddress = new Customer("George", Arrays.asList(
        new PhoneBill("2101234567", BigDecimal.valueOf(33)),
        new PhoneBill("3101234567", BigDecimal.valueOf(-1)),
        new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
        new PhoneBill("4101234567", BigDecimal.valueOf(15))));

    private final Customer customerWithAddressAndNoStreet = new Customer("Christos", Arrays.asList(
        new PhoneBill("2131234567", BigDecimal.valueOf(50)),
        new PhoneBill("3131234567", BigDecimal.valueOf(30)),
        new PhoneBill("4131234567", BigDecimal.valueOf(20))),
        addressWithNoStreet);

    private final Customer customerWithAddressAndStreet = new Customer("Spyros", Arrays.asList(
        new PhoneBill("2151234567", BigDecimal.valueOf(150)),
        new PhoneBill("3151234567", BigDecimal.valueOf(80)),
        new PhoneBill("4151234567", BigDecimal.valueOf(155))),
        address);

    public Customer provideCustomerWithAddressAndStreet(){
        return customerWithAddressAndStreet;
    }

    public Customer provideCustomerWithAddressAndNoStreet() {
        return customerWithAddressAndNoStreet;
    }

    public Customer provideCustomerWithOutAddress() {
        return customerWithoutAddress;
    }
}
