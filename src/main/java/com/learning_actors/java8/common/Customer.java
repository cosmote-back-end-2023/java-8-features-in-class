package com.learning_actors.java8.common;

import java.util.List;
import java.util.Objects;

public final class Customer {
    private final String name;
    private final List<PhoneBill> phoneBills;
    private Address address;

    public Customer(String name, List<PhoneBill> phoneBills) {
        this.name = Objects.requireNonNull(name, "Customer Name must be provided");
        this.phoneBills = Objects.requireNonNull(phoneBills, "Customer phone bills must be provided");
    }

    public Customer(String name, List<PhoneBill> phoneBills, Address address) {
        this.name = Objects.requireNonNull(name, "Customer Name must be provided");
        this.phoneBills = Objects.requireNonNull(phoneBills, "Customer phone bills must be provided");
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public List<PhoneBill> getPhoneBills() {
        return phoneBills;
    }

    public Address getAddress() {
        return address;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Customer{");
        sb.append("name='").append(name).append('\'');
        sb.append(", phoneBills=").append(phoneBills);
        sb.append(", address=").append(address);
        sb.append('}');
        return sb.toString();
    }
}
