package com.learning_actors.java8.common;

public class Address {

    private String postalCode;
    private String city;
    private String state;
    private Street street;


    public Address(String postalCode, String city, String state, Street street) {
        this.postalCode = postalCode;
        this.city = city;
        this.state = state;
        this.street = street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public Street getStreet() {
        return street;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Address{");
        sb.append("postalCode='").append(postalCode).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", state=").append(state);
        sb.append(", street=").append(street);
        sb.append('}');
        return sb.toString();
    }
}
