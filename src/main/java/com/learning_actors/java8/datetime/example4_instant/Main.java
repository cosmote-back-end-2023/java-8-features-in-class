package com.learning_actors.java8.datetime.example4_instant;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class Main {

    public static void main(String[] args) {
        Instant now = Instant.now();

        System.out.println(now);
        System.out.println(now.minus(1, ChronoUnit.DAYS));
        System.out.println(now.plus(1, ChronoUnit.HOURS));
        System.out.println(now.plus(1, ChronoUnit.MINUTES));
        System.out.println(now.plusSeconds(15));
        System.out.println(now.plusMillis(15));
        System.out.println(now.minusNanos(50));

//      System.out.println(now.minus(1, ChronoUnit.MONTHS)); // Does not apply, each month is not always X seconds
//      System.out.println(now.minus(1, ChronoUnit.WEEKS)); //  Does not apply, each week is not always X seconds
//      System.out.println(now.minus(1, ChronoUnit.YEARS)); //  Does not apply, each year is not always X seconds

        System.out.println(now.getEpochSecond());
        System.out.println(now.toEpochMilli());

        Instant pastInstant = Instant.now().minusSeconds(200);
        System.out.println(now.equals(pastInstant)); //false
        System.out.println(now.isAfter(pastInstant)); //true
        System.out.println(now.isBefore(pastInstant)); //false
    }

}
