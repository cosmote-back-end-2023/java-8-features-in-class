package com.learning_actors.java8.datetime.example8_datetimeformatter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Main {

    public static void main(String[] args) {
        String input = "25-December-2019";
        DateTimeFormatter customFormatter1 = DateTimeFormatter.ofPattern("dd-MMMM-yyyy");
        LocalDate date = LocalDate.parse(input, customFormatter1);
        System.out.println(date);


        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter customFormatter2 = DateTimeFormatter.ofPattern("h:mm d MMMM, yyyy");
        System.out.println(localDateTime.format(customFormatter2));
        System.out.println(localDateTime.format(DateTimeFormatter.BASIC_ISO_DATE));
        System.out.println(localDateTime.format(DateTimeFormatter.ISO_DATE_TIME));
    }

}
