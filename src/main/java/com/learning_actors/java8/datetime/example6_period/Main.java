package com.learning_actors.java8.datetime.example6_period;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

public class Main {

    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        LocalDate myBirthday = LocalDate.of(1983, Month.MARCH, 28);

        Period period = Period.between(myBirthday, today);

        System.out.printf("I am %s years, %s months and %s days old.%n", period.getYears(), period.getMonths(), period.getDays());
        System.out.printf("I am %s months old%n", period.toTotalMonths());
    }

}
