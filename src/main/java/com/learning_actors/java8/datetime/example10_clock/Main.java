package com.learning_actors.java8.datetime.example10_clock;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class Main {

    public static void main(String[] args) {
        Clock fixedClock = Clock.fixed(Instant.parse("2019-06-21T13:30:00.00Z"), ZoneId.systemDefault());

        System.out.println(LocalDate.now(fixedClock));
        System.out.println(LocalDateTime.now(fixedClock));
        System.out.println(Instant.now(fixedClock));
    }

}
