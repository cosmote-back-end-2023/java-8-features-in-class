package com.learning_actors.java8.datetime.example7_temporal_adjusters;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

public class Main {

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();

        TemporalAdjuster nextWednesday = TemporalAdjusters.next(DayOfWeek.WEDNESDAY);
        TemporalAdjuster firstDayOfMonth = TemporalAdjusters.firstDayOfMonth();
        TemporalAdjuster firstDayOfNextMonth = TemporalAdjusters.firstDayOfNextMonth();

        System.out.println(now.with(nextWednesday));
        System.out.println(now.with(firstDayOfMonth));
        System.out.println(now.with(firstDayOfNextMonth));
    }

}
