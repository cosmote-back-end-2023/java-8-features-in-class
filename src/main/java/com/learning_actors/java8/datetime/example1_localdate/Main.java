package com.learning_actors.java8.datetime.example1_localdate;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        LocalDate date1 = LocalDate.of(2021, 5, 13);
        LocalDate date2 = LocalDate.parse("2021-05-13");

        LocalDate yesterday = today.minusDays(1);
        LocalDate previousWeek = today.minusWeeks(1);
        LocalDate previousMonth = today.minusMonths(1);
        LocalDate previousYear = today.minusYears(1);

        System.out.println(today);
        System.out.println(yesterday);
        System.out.println(previousWeek);
        System.out.println(previousMonth);
        System.out.println(previousYear);

        System.out.println(today.getDayOfMonth());  // [1 - 31]
        System.out.println(today.getDayOfWeek());  // [MONDAY, TUESDAY, ..., SUNDAY]
        System.out.println(today.getDayOfYear());  // [1 - 366]

        System.out.println(today.isAfter(yesterday)); // true
        System.out.println(today.isBefore(yesterday)); // false
        System.out.println(today.isEqual(yesterday)); // false
    }

}
