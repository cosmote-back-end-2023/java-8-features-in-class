package com.learning_actors.java8.datetime.example9_timezones;

import java.time.Instant;
import java.time.ZoneId;

public class Main {

    public static void main(String[] args) {
        ZoneId athensZone = ZoneId.of("Europe/Athens");
        ZoneId chicagoZone = ZoneId.of("America/Chicago");
        ZoneId tokyoZone = ZoneId.of("Asia/Tokyo");

        Instant now = Instant.now();
        System.out.println("The current time in Athens is: " + now.atZone(athensZone));
        System.out.println("The current time in Chicago is: " + now.atZone(chicagoZone));
        System.out.println("The current time in Tokyo is: " + now.atZone(tokyoZone));
    }

}
