package com.learning_actors.java8.datetime.example2_localtime;

import java.time.LocalTime;

public class Main {

    public static void main(String[] args) {
        LocalTime now = LocalTime.now();
        LocalTime time1 = LocalTime.of(20, 12);
        LocalTime time2 = LocalTime.of(20, 12, 8, 1);
        LocalTime time3 = LocalTime.parse("20:12:00");

        LocalTime nextHour = now.plusHours(1);
        LocalTime previousMinute = now.minusMinutes(1);
        LocalTime previousSecond = now.minusSeconds(1);
        LocalTime previousNanosecond = now.minusNanos(1);

        System.out.println(now);
        System.out.println(nextHour);
        System.out.println(previousMinute);
        System.out.println(previousSecond);
        System.out.println(previousNanosecond);

        System.out.println(now.getHour());    // [0 - 23]
        System.out.println(now.getMinute());  // [0 - 59]
        System.out.println(now.getSecond());  // [0 - 59]
        System.out.println(now.getNano());    // [0 - 999,999,999]

        System.out.println(now.isAfter(nextHour)); // false
        System.out.println(now.isBefore(nextHour)); // true
        System.out.println(now.equals(nextHour)); // false

        LocalTime plus24Hours = now.plusHours(24);
        System.out.println(now.equals(plus24Hours)); // true
    }

}
