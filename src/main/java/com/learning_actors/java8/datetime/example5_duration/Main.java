package com.learning_actors.java8.datetime.example5_duration;

import java.time.Duration;
import java.time.Instant;

public class Main {

    public static void main(String[] args) {
        Instant startTimestamp = Instant.parse("2021-05-13T09:30:00.00Z");
        Instant endTimestamp = Instant.parse("2021-05-13T13:30:00.00Z");

        Duration classDuration = Duration.between(startTimestamp, endTimestamp);

        System.out.println(classDuration.toDays());
        System.out.println(classDuration.toHours());
        System.out.println(classDuration.toMinutes());
        System.out.println(classDuration.getSeconds());
        System.out.println(classDuration.toMillis());
        System.out.println(classDuration.toNanos());

    }

}
