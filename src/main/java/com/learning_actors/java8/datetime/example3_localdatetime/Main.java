package com.learning_actors.java8.datetime.example3_localdatetime;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime yesterday = now.minusDays(1);
        LocalDateTime previousMonth = now.minusMonths(1);
        LocalDateTime previousWeek = now.minusWeeks(1);
        LocalDateTime previousYear = now.minusYears(1);
        LocalDateTime nextHour = now.plusHours(1);
        LocalDateTime nextMinute = now.plusMinutes(1);
        LocalDateTime nextSecond = now.plusSeconds(1);
        LocalDateTime nextNanosecond = now.minusNanos(1);

        System.out.println(now);
        System.out.println(yesterday);
        System.out.println(previousMonth);
        System.out.println(previousWeek);
        System.out.println(previousYear);
        System.out.println(nextHour);
        System.out.println(nextMinute);
        System.out.println(nextSecond);
        System.out.println(nextNanosecond);

        System.out.println(now.getDayOfMonth());  // [1 - 31]
        System.out.println(now.getDayOfWeek());  // [MONDAY, TUESDAY, ..., SUNDAY]
        System.out.println(now.getDayOfYear());  // [1 - 366]
        System.out.println(now.getHour()); // [0 - 23]
        System.out.println(now.getMinute()); // [0 - 59]
        System.out.println(now.getSecond()); // [0 - 59]
        System.out.println(now.getNano()); // [0 - 999,999,999]

        System.out.println(now.isAfter(yesterday)); // true
        System.out.println(now.isBefore(yesterday)); // false
        System.out.println(now.isEqual(yesterday)); // false
    }

}
