package com.learning_actors.java8.stream.example08_peek;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public long getNumberOfNegativePhoneBills(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .filter(PhoneBill::isNegative)
                .peek(phoneBill -> System.out.println("DEBUG Negative phone bill found, phone number: " + phoneBill.getPhoneNumber()))
                .count();
    }

}
