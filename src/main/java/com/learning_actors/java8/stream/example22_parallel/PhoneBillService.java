package com.learning_actors.java8.stream.example22_parallel;

import java.math.BigDecimal;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public BigDecimal calculateTotalCost(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .map(PhoneBill::getCost)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal calculateTotalCostInParallel(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .parallel()
                .map(PhoneBill::getCost)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
