package com.learning_actors.java8.stream.example06_limit;

import java.util.List;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public List<PhoneBill> get3PhoneBills(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .limit(3)
                .collect(Collectors.toList());
    }

}
