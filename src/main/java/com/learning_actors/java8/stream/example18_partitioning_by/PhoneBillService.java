package com.learning_actors.java8.stream.example18_partitioning_by;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public Map<Boolean, List<PhoneBill>> getPhoneBillsPartitionedByNegativeCost(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .collect(Collectors.partitioningBy(PhoneBill::isNegative));
    }

}
