package com.learning_actors.java8.stream.example17_joining;

import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public String getAllPhoneNumberWithComma(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .map(PhoneBill::getPhoneNumber)
                .collect(Collectors.joining(","));
    }

}
