package com.learning_actors.java8.stream.example01_intro;

import java.util.List;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public interface PhoneBillService {

    List<PhoneBill> findAllPhoneBillsWithNegativeCostOrdered(Customer customer);

}
