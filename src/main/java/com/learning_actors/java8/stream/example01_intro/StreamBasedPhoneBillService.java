package com.learning_actors.java8.stream.example01_intro;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class StreamBasedPhoneBillService implements PhoneBillService {

    public List<PhoneBill> findAllPhoneBillsWithNegativeCostOrdered(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .filter(PhoneBill::isNegative)
                .sorted(Comparator.comparing(PhoneBill::getCost))
                .collect(Collectors.toList());
    }

}
