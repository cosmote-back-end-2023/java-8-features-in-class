package com.learning_actors.java8.stream.example01_intro;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class NonStreamBasedPhoneBillService implements PhoneBillService {

    public List<PhoneBill> findAllPhoneBillsWithNegativeCostOrdered(Customer customer) {
        List<PhoneBill> phoneBillsWithNegativeCost = new LinkedList<>();

        for (PhoneBill phoneBill : customer.getPhoneBills()) {
            if (phoneBill.isNegative()) {
                phoneBillsWithNegativeCost.add(phoneBill);
            }
        }

        phoneBillsWithNegativeCost.sort(new Comparator<PhoneBill>() {
            @Override
            public int compare(PhoneBill o1, PhoneBill o2) {
                return o1.getCost().compareTo(o2.getCost());
            }
        });

        return phoneBillsWithNegativeCost;
    }

}
