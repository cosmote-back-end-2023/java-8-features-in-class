package com.learning_actors.java8.stream.example20_collectors_max_min_by;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public Optional<PhoneBill> getMostExpensivePhoneBill(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .collect(Collectors.maxBy(Comparator.comparing(PhoneBill::getCost)));//Prefer java.util.stream.Stream.max()
    }

    //Advanced
    public Map<String, PhoneBill> getMostExpensivePhoneBillPerPhoneNumber(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .collect(Collectors.groupingBy(PhoneBill::getPhoneNumber,
                        Collectors.collectingAndThen(
                                Collectors.maxBy(Comparator.comparing(PhoneBill::getCost)),
                                Optional::get//It's required because maxBy returns an Optional
                        )
                ));
    }

}
