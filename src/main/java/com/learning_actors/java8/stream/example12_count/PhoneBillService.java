package com.learning_actors.java8.stream.example12_count;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public long getNumberOfNegativePhoneBills(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .filter(PhoneBill::isNegative)
                .count();
    }

}
