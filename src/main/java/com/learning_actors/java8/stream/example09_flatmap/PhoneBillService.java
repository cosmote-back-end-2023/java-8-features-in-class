package com.learning_actors.java8.stream.example09_flatmap;

import java.util.List;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public List<PhoneBill> getAllPhoneBills(List<Customer> customers) {
        return customers
                .stream()
                .map(Customer::getPhoneBills)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

}
