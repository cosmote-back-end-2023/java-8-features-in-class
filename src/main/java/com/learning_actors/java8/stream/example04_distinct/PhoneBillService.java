package com.learning_actors.java8.stream.example04_distinct;

import java.util.List;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public List<String> getAllPhoneNumbers(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .map(PhoneBill::getPhoneNumber)
                .distinct()
                .collect(Collectors.toList());
    }

}
