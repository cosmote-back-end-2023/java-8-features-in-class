package com.learning_actors.java8.stream.example05_sorted;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public List<PhoneBill> getAllPhoneBillsSortedByCost(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .sorted(Comparator.comparing(PhoneBill::getCost))
                .collect(Collectors.toList());
    }

}
