package com.learning_actors.java8.stream.example16_grouping_by;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public Map<String, List<PhoneBill>> getPhoneBillsGroupingByPhoneNumber(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .collect(Collectors.groupingBy(PhoneBill::getPhoneNumber));
    }

    public Map<String, Long> getNumberOfBillsGroupingByPhoneNumber(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .collect(Collectors.groupingBy(PhoneBill::getPhoneNumber, Collectors.counting()));
    }

    //Advanced
    public Map<String, BigDecimal> getTotalCostOfBillsGroupingByPhoneNumber(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .collect(
                        Collectors.groupingBy(PhoneBill::getPhoneNumber,
                                Collectors.mapping(PhoneBill::getCost,
                                        Collectors.reducing(BigDecimal.ZERO, BigDecimal::add)
                                )
                        )
                );
    }

}
