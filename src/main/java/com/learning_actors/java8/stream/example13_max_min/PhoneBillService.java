package com.learning_actors.java8.stream.example13_max_min;

import java.util.Comparator;
import java.util.Optional;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public Optional<PhoneBill> findMostExpensivePhoneBill(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .max(Comparator.comparing(PhoneBill::getCost));
    }

    public Optional<PhoneBill> findLessExpensivePhoneBill(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .min(Comparator.comparing(PhoneBill::getCost));
    }

}
