package com.learning_actors.java8.stream.example21_collectors_mapping;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public List<String> getPhoneNumbers(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .collect(Collectors.mapping(PhoneBill::getPhoneNumber, Collectors.toList()));//Prefer java.util.stream.Stream.map()
    }

    //Advanced
    public Map<String, List<BigDecimal>> getBillCostsPerPhoneNumbers(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .collect(Collectors.groupingBy(PhoneBill::getPhoneNumber, Collectors.mapping(PhoneBill::getCost, Collectors.toList())));
    }

}
