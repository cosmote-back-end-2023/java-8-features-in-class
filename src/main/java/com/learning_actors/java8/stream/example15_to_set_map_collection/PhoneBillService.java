package com.learning_actors.java8.stream.example15_to_set_map_collection;

import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public Set<String> getAllPhoneNumbersAsSet(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .map(PhoneBill::getPhoneNumber)
                .collect(Collectors.toSet());
    }

    public LinkedList<String> getAllPhoneNumbersAsLinkedList(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .map(PhoneBill::getPhoneNumber)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public Map<String, PhoneBill> getAllPhoneBillsByPhoneNumber(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .collect(Collectors.toMap(PhoneBill::getPhoneNumber, Function.identity()));//Please note, no duplicates are allowed!
    }

}
