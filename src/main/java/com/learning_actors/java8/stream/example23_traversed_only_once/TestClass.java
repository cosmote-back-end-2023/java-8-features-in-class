package com.learning_actors.java8.stream.example23_traversed_only_once;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestClass {

    //Never do that!
    public void tryToTraverseStreamMultipleTimes(List<String> list) {
        Stream<String> myStream = list.stream();

        long count = myStream.count();//Terminal Operator, stream consumed

        //try to consume it again
        List<String> myList = myStream.map(String::toUpperCase).collect(Collectors.toList());
    }

}
