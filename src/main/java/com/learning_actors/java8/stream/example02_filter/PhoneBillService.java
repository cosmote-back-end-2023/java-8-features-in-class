package com.learning_actors.java8.stream.example02_filter;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    private static final BigDecimal COST_LIMIT = BigDecimal.valueOf(200);

    public List<PhoneBill> getPhoneBillsThatExceedCostLimit(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .filter(phoneBill -> phoneBill.getCost().compareTo(COST_LIMIT) > 0)
                .collect(Collectors.toList());
    }

}
