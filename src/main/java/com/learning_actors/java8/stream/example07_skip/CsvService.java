package com.learning_actors.java8.stream.example07_skip;

import java.util.List;
import java.util.stream.Collectors;

public class CsvService {

    public List<String> getCsvContentWithoutTheHeader(List<String> csvContent) {
        return csvContent
                .stream()
                .skip(1)
                .collect(Collectors.toList());
    }
}
