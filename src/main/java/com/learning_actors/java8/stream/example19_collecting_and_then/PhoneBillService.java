package com.learning_actors.java8.stream.example19_collecting_and_then;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public List<String> getPhoneNumbersUnmodifiableList(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .map(PhoneBill::getPhoneNumber)
                .collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));
    }

}
