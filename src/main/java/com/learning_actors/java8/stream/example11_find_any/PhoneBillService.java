package com.learning_actors.java8.stream.example11_find_any;

import java.util.Optional;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public Optional<PhoneBill> findAnyNegativeBill(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .filter(PhoneBill::isNegative)
                .findAny();
    }

}
