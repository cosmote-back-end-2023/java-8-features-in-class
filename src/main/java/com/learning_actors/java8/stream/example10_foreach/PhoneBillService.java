package com.learning_actors.java8.stream.example10_foreach;

import com.learning_actors.java8.common.Customer;

public class PhoneBillService {

    public void printAllPhoneNumbers(Customer customer) {
        customer.getPhoneBills()
                .stream()
                .forEach(phoneBill -> System.out.println("Phone Number: " + phoneBill.getPhoneNumber()));
    }

}
