package com.learning_actors.java8.stream.example03_map;

import java.util.List;
import java.util.stream.Collectors;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public List<String> getAllPhoneNumbers(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .map(PhoneBill::getPhoneNumber)
                .collect(Collectors.toList());
    }

}
