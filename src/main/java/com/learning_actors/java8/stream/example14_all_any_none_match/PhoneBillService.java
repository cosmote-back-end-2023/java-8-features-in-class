package com.learning_actors.java8.stream.example14_all_any_none_match;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillService {

    public boolean areAllPhoneBillsNegative(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .allMatch(PhoneBill::isNegative);
    }

    public boolean isAnyPhoneBillNegative(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .anyMatch(PhoneBill::isNegative);
    }

    public boolean areAllPhoneBillsNotNegative(Customer customer) {
        return customer.getPhoneBills()
                .stream()
                .noneMatch(PhoneBill::isNegative);
    }

}
