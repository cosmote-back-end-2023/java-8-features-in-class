package com.learning_actors.java13;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class SwithYieldExamplePreview {
    public static void main(String[] args) {
        boolean isWorkingDay;
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();


        isWorkingDay = switch (dayOfWeek) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> {
                yield true;
            }
            case SATURDAY, SUNDAY -> {
                yield false;
            }
        };

        //alternative writing
        /*
        isWorkingDay = switch (dayOfWeek) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY: yield true;
            case SATURDAY, SUNDAY: yield false;
        };
         */

        System.out.println(isWorkingDay);
    }
}
