package com.learning_actors.java15;

sealed interface Shape permits Circle, Rectangle {
}
