package com.learning_actors.java15;


public class SealedClassPreview {
    public static void main(String[] args) {
        Rectangle r = new Rectangle(33.33, 25.23);
        Circle c = new Circle(6.25);

        getShapeSpecifics(r);
        getShapeSpecifics(c);
    }

    private static void getShapeSpecifics(Shape shape) {
        if (shape instanceof Circle c) {
            System.out.println("Circle has a radius of: " + c.getRadius());
        } else if (shape instanceof Rectangle r) {
            System.out.println("Rectangle has a width of: " + r.getWidth());
        }

    }

}
