package com.learning_actors.assignment;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Assignment {

    String jfk = "We choose to go to the moon. " +
            "We choose to go to the moon in this decade and do the other things, " +
            "not because they are easy, " +
            "but because they are hard, " +
            "because that goal will serve to organize and measure the best of our energies and skills, " +
            "because that challenge is one that we are willing to accept, " +
            "one we are unwilling to postpone, " +
            "and one which we intend to win, " +
            "and the others, too.";

    public String[] getWords() {
        return jfk
                .toLowerCase()
                .replace(".", "")
                .replace(",", "")
                .split(" ");
    }

    private void printWordCount() {
        long count = Arrays.stream(getWords())
                .count();
        System.out.printf("Υπάρχουν: %d λέξεις \n", count);
    }

    public void printWordLengths() {}

    private void printWordFrequencies() {}

    private void printLengthiestWords() {}

    private void printMostFrequentWords() {}

    public void printDistinctAndDuplicateWordsInTheOrderEncountered() {}

    public void printDistinctWordsInNaturalOrder() {}

    public void printDistinctWordsHavingFrequencyOtherThanThree() {}

    private void printUniqueWordsInNaturalOrderHavingLengthLessThanThreeLetters() {}

    private void printWordsGroupedByLength() {}

    private void printWordsGroupedByFrequency() {}

    private void printWordsGroupedByLengthAndAggregateFrequencies() {}

    public static void main(String[] args) {
        Assignment assignment = new Assignment();

        //Παράδειγμα:
        System.out.println("Τύπωσε τον αριθμό των λέξεων του κειμένου");
        assignment.printWordCount();

        System.out.println("Τύπωσε ποιο είναι το μήκος της κάθε λέξης του κειμένου.");
        assignment.printWordLengths();

        System.out.println("Τύπωσε πόσες φορές εμφανίζεται η κάθε λέξη στο κείμενο.");
        assignment.printWordFrequencies();

        System.out.println("Τύπωσε τη μεγαλυτερη σε μήκος λέξη και το μήκος της.");
        assignment.printLengthiestWords();

        System.out.println("Τύπωσε τη λέξη που εμφανίζεται περισσότερες φορές στο κείμενο και πόσες είναι.");
        assignment.printMostFrequentWords();

        System.out.println("Τύπωσε τις διακριτές λέξεις που εμφανίζονται στο κείμενο και έπειτα τις διπλότυπες με τη σειρά εμφάνισης τους");
        assignment.printDistinctAndDuplicateWordsInTheOrderEncountered();

        System.out.println("Τύπωσε τις διακριτές λέξεις που εμφανίζονται στο κείμενο με λεξικογραφική (φυσική) σειρά");
        assignment.printDistinctWordsInNaturalOrder();

        System.out.println("Τύπωσε τις διακριτές λέξεις με τη σειρά που εμφανίζονται στο κείμενο αφου αφαιρεσεις (φιλτραρεις) οσες εμφανίζονται ακριβώς 3 φορές");
        assignment.printDistinctWordsHavingFrequencyOtherThanThree();

        System.out.println("Τύπωσε με λεξικογραφική (φυσική) σειρά τις διακριτές λέξεις που εμφανίζονται στο κείμενο αφου αφαιρεσεις (φιλτραρεις) οσες εχουν μήκος μεγαλύτερο απο 3 γράμματα");
        assignment.printUniqueWordsInNaturalOrderHavingLengthLessThanThreeLetters();

        System.out.println("Τύπωσε αφου ομαδοποιήσεις (groupBy) τις λέξεις με βαση το μήκος τους");
        assignment.printWordsGroupedByLength();

        System.out.println("Τύπωσε αφου ομαδοποιήσεις (groupBy) τις λέξεις με βαση τη συχνότητα εμφάνισης τους");
        assignment.printWordsGroupedByFrequency();

        System.out.println("Τύπωσε αφου ομαδοποιήσεις (groupBy) τις λέξεις με βαση το μήκος τους και αθροίσεις όλες τις συχνοτητες");
        assignment.printWordsGroupedByLengthAndAggregateFrequencies();
    }

}
