package com.learning_actors.java14;

public class InstanceOfPatternMatchingPreview {

    public static void main(String[] args) {
        Object obj = "s";

        /* Previous java versions
        if (obj instanceof String) {
            String str = (String) obj;
            System.out.println("Length of the string: " + str.length());
        } else {
            System.out.println("Not a String");
        }*/

        if (obj instanceof String str) {
            // If obj is a String, str is bound to the value of obj
            System.out.println("Length of the string: " + str.length());
        } else {
            System.out.println("Not a String");
        }


    }
}
