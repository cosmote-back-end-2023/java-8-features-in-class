package com.learning_actors.java14;

public class RecordsPreview {

    private record CustomerRecord(int id, String address, int age) {};
    public static void main(String[] args) {

        CustomerRecord customerRecord = new CustomerRecord(1, "Galatsiou 25", 43);
        CustomerRecord customerRecord2 = new CustomerRecord(2, "Galatsiou 27", 44);

        System.out.println(customerRecord);
        System.out.println(customerRecord.equals(customerRecord2));
    }
}
