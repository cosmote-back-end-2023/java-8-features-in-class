package com.learning_actors.java12;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class SwitchAssignmentPreview {
    public static void main(String[] args) {
        boolean isWorkingDay;
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        /*
        //Prior to Java 11


        switch (dayOfWeek) {
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
            case FRIDAY:
                isWorkingDay = true;
                break;
            case SATURDAY:
            case SUNDAY:
                isWorkingDay = false;
        }*/

        isWorkingDay = switch (dayOfWeek) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> true;
            case SATURDAY, SUNDAY -> false;
        };

        System.out.println(isWorkingDay);
    }
}
