package com.learning_actors.java12;

import java.text.NumberFormat;
import java.util.Locale;

public class CompactNumberFormatterExample {
    public static void main(String[] args) {
        NumberFormat likesShort =
                NumberFormat.getCompactNumberInstance(new Locale("en", "US"), NumberFormat.Style.SHORT);
        likesShort.setMaximumFractionDigits(2);
        System.out.println(likesShort.format(1443));

        NumberFormat likesLong =
                NumberFormat.getCompactNumberInstance(new Locale("en", "US"), NumberFormat.Style.LONG);
        likesLong.setMaximumFractionDigits(3);
        System.out.println(likesLong.format(1443));
    }
}
