package com.learning_actors.java12;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.TestDataProvider;

import java.util.stream.Collectors;

public class TeeingCollector {

    private static final TestDataProvider testDataProvider = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {

        Customer customerWithAddressAndStreet = testDataProvider.provideCustomerWithAddressAndStreet();

        double meanPhoneBillCost = customerWithAddressAndStreet.getPhoneBills().stream()
                .collect(Collectors.teeing(Collectors.summingDouble(pb -> pb.getCost().doubleValue()),
                        Collectors.counting(), (sum, count) -> sum / count));

        System.out.println("Mean cost is " + meanPhoneBillCost);
    }
}
