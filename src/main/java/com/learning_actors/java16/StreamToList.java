package com.learning_actors.java16;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import java.math.BigDecimal;
import java.util.Arrays;

public class StreamToList {

    public static void main(String[] args) {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("2111234567", BigDecimal.valueOf(200)),
                new PhoneBill("2121234567", BigDecimal.valueOf(201)),
                new PhoneBill("2121234567", BigDecimal.valueOf(202))
        ));

        /* Previous Java versions
        System.out.println(customer.getPhoneBills()
                .stream()
                .map(PhoneBill::getPhoneNumber)
                .distinct()
                .collect(Collectors.toList()));*/

        //Java 16
        System.out.println(customer.getPhoneBills()
                .stream()
                .map(PhoneBill::getPhoneNumber)
                .distinct()
                .toList());

    }
}
