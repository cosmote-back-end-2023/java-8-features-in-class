package com.learning_actors.java16.default_method_invocation;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import static java.lang.ClassLoader.getSystemClassLoader;

public class InvokeDefaultMethod {
    public static void main(String[] args) throws Exception {
        Object proxy = Proxy.newProxyInstance(getSystemClassLoader(), new Class<?>[] {MyInterface.class },
                (prox, method, arguments) -> {
                    if (method.isDefault()) {
                        return InvocationHandler.invokeDefault(prox, method, arguments);
                    }
                    return method.invoke(prox, arguments);
                }
        );


        Method method = proxy.getClass().getMethod("hello");
        System.out.println(method.invoke(proxy));
    }

}
