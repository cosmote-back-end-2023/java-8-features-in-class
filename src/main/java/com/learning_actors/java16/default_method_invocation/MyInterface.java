package com.learning_actors.java16.default_method_invocation;

public interface MyInterface {
    default String hello() {
        return "world";
    }
}
