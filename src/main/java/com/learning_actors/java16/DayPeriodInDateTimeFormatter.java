package com.learning_actors.java16;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DayPeriodInDateTimeFormatter {

    public static void main(String[] args) {
        LocalTime date = LocalTime.parse("17:25:28.123591");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("H B");
        System.out.println(date.format(formatter));

        LocalDateTime dateTime = LocalDateTime.now();
        formatter = DateTimeFormatter.ofPattern("dd HH:mm BBBB");
        System.out.println(dateTime.format(formatter));

        formatter = DateTimeFormatter.ofPattern("HH:mm BBBB");
        System.out.println(date.format(formatter));

    }

}
