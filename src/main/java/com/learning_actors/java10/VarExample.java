package com.learning_actors.java10;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.TestDataProvider;

import java.util.ArrayList;
import java.util.Map;

public class VarExample {

    private static final TestDataProvider testDataProvider = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {

        var  myInt = 5;

        Customer customerWithAddressAndStreet = testDataProvider.provideCustomerWithAddressAndStreet();
        Customer customerWithOutAddress = testDataProvider.provideCustomerWithOutAddress();
        Customer customerWithAddressAndNoStreet = testDataProvider.provideCustomerWithAddressAndNoStreet();

        var phoneBillList = customerWithAddressAndNoStreet.getPhoneBills();
        System.out.println(phoneBillList.getClass().getName());
        System.out.println(phoneBillList);

        for (var pb : customerWithOutAddress.getPhoneBills()) {
            System.out.println(pb.getPhoneNumber());
        }

        var customerMap = Map.of(
            1, customerWithOutAddress,
            2, customerWithAddressAndNoStreet);

        //below block can be replaced with var:
        /*for (Map.Entry<Integer, Customer> customer: customerMap.entrySet()) {
            System.out.println(customer);
        }*/

        for (var customer: customerMap.entrySet()) {
            System.out.println(customer);
        }

        var customers = new ArrayList<>();
        System.out.println(customers.getClass().getName());
        customers.add(customerWithOutAddress);
        customers.add(customerWithAddressAndNoStreet);
        System.out.println(customers);

        /*
        //Dangerous: infers as List<Object>
        var users = new ArrayList<>();
        users.add(customerWithAddressAndStreet);
        users.add(customerWithAddressAndStreet.getAddress());
        System.out.println(users);

        //OK: infers as List<Customer>
        var list = List.of(customerWithAddressAndStreet);

        */


        //following won't compile
/*        var aList = List.of(customerWithOutAddress, customerWithAddressAndNoStreet);
        aList = phoneBillList;*/

    }


}
