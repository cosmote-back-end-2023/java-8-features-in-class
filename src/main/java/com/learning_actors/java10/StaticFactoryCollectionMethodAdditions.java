package com.learning_actors.java10;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;
import com.learning_actors.java8.common.TestDataProvider;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public class StaticFactoryCollectionMethodAdditions {

    private static final TestDataProvider testDataProvider = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {

        Customer customerWithAddressAndStreet = testDataProvider.provideCustomerWithAddressAndStreet();
        Customer customerWithOutAddress = testDataProvider.provideCustomerWithOutAddress();
        Customer customerWithAddressAndNoStreet = testDataProvider.provideCustomerWithAddressAndNoStreet();


        List<PhoneBill> phoneBills = customerWithAddressAndStreet.getPhoneBills();
        List<PhoneBill> unmodifiablePhoneBillList = List.copyOf(phoneBills);
        System.out.println(unmodifiablePhoneBillList);

        try {
            unmodifiablePhoneBillList.add(new PhoneBill("2107235555", BigDecimal.valueOf(150.0)));
        } catch (UnsupportedOperationException e) {
            System.out.println("UnsupportedOperationException was thrown");
        }

        Set<PhoneBill> unmodifiablePhoneBillSet = Set.copyOf(unmodifiablePhoneBillList);
        try {
            unmodifiablePhoneBillSet.add(new PhoneBill("2107235555", BigDecimal.valueOf(150.0)));
        } catch (UnsupportedOperationException e) {
            System.out.println("UnsupportedOperationException was thrown");
        }

    }

}
