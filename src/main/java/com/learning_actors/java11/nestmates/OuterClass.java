package com.learning_actors.java11.nestmates;

public class OuterClass {
    private int privateField = 42;

    public class InnerClass {
        public void accessOuterPrivateField() {
            System.out.println("InnerClass accessing private field of OuterClass: " + privateField);
        }
    }

    class AnotherClassInTheSameNest {
        public void accessOuterPrivateField(OuterClass outer) {
            // Accessing private field of OuterClass from another class in the same nest
            //If we execute this code using earlier versions of Java than Java 11, the compiler will create a bridge
            // method to call the private method, but using Java 11 there is no need of a bridge method
            // to call private members.
            System.out.println("AnotherClassInTheSameNest accessing private field of OuterClass: " + outer.privateField);
        }
    }
}





