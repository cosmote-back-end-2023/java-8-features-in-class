package com.learning_actors.java11.nestmates;

import java.util.Arrays;

public class NestmateExample {
    public static void main(String[] args) {
        OuterClass outer = new OuterClass();
        OuterClass.InnerClass inner = outer.new InnerClass();
        OuterClass.AnotherClassInTheSameNest anotherClass = outer.new AnotherClassInTheSameNest();

        // Accessing private field from InnerClass
        inner.accessOuterPrivateField();

        // Accessing private field from AnotherClassInTheSameNest
        anotherClass.accessOuterPrivateField(outer);
        //Get Nest Host Name
        System.out.println(OuterClass.class.getNestHost());
        // Get Nest Members
        System.out.println(Arrays.toString(OuterClass.class.getNestMembers()));
        // Check whether a class is nestmate
        System.out.println(OuterClass.class.isNestmateOf(OuterClass.AnotherClassInTheSameNest.class));

    }


}