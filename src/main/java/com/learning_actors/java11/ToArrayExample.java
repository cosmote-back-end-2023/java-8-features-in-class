package com.learning_actors.java11;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.TestDataProvider;

import java.util.Arrays;
import java.util.List;

public class ToArrayExample {
    public static void main(String[] args) {
        Customer customerWithAddressAndNoStreet =
                TestDataProvider.getTestDataProviderInstance().provideCustomerWithAddressAndNoStreet();
        Customer customerWithAddressAndStreet =
                TestDataProvider.getTestDataProviderInstance().provideCustomerWithAddressAndStreet();

        List<Customer> customers = List.of(customerWithAddressAndStreet, customerWithAddressAndNoStreet);

        Customer[] array = customers.toArray(Customer[]::new);
        Arrays.stream(array).forEach(System.out::println);
    }
}
