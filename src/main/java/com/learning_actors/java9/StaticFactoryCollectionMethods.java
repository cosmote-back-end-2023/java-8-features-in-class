package com.learning_actors.java9;

import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.TestDataProvider;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class StaticFactoryCollectionMethods {

    private static final TestDataProvider testDataProvider = TestDataProvider.getTestDataProviderInstance();

    public static void main(String[] args) {

        Customer customerWithAddressAndStreet = testDataProvider.provideCustomerWithAddressAndStreet();
        Customer customerWithOutAddress = testDataProvider.provideCustomerWithOutAddress();
        Customer customerWithAddressAndNoStreet = testDataProvider.provideCustomerWithAddressAndNoStreet();

        List<Customer> unmodifiableCustomerList = List.of(customerWithOutAddress, customerWithAddressAndNoStreet);
        System.out.println(unmodifiableCustomerList);

        Set<Customer> unmodifiableCustomerSet = Set.of(customerWithOutAddress, customerWithAddressAndNoStreet);
        System.out.println(unmodifiableCustomerList);

        Map<Integer, Customer> unmodifiableCustomerMap = Map.of(
            1, customerWithOutAddress,
            2, customerWithAddressAndNoStreet);
        System.out.println(unmodifiableCustomerList);
        System.out.println(unmodifiableCustomerMap);

        //the below statement will throw an UnsupportedOperationException if called after initialization
/*        try {*/
            unmodifiableCustomerList.add(customerWithAddressAndStreet);
            unmodifiableCustomerSet.add(customerWithAddressAndStreet);
            unmodifiableCustomerMap.put(3, customerWithAddressAndStreet);
/*        } catch (UnsupportedOperationException e) {
            System.out.println("UnsupportedOperationException was thrown");
        }*/



    }

}
