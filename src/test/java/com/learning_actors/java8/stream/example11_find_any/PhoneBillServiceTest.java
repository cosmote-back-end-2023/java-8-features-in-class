package com.learning_actors.java8.stream.example11_find_any;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_find_any_negative_bill() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(-10)),
                new PhoneBill("3101234567", BigDecimal.valueOf(-5)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        Optional<PhoneBill> negativeBill = phoneBillService.findAnyNegativeBill(customer);

        assertTrue(negativeBill.isPresent());
        assertTrue(negativeBill.get().getCost().compareTo(BigDecimal.ZERO) < 0);
    }

    @Test
    public void should_handle_case_without_negative_bill() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(10)),
                new PhoneBill("3101234567", BigDecimal.valueOf(5)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        Optional<PhoneBill> negativeBill = phoneBillService.findAnyNegativeBill(customer);

        assertFalse(negativeBill.isPresent());
    }

}