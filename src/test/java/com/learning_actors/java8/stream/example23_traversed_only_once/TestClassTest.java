package com.learning_actors.java8.stream.example23_traversed_only_once;

import java.util.Arrays;
import org.junit.Test;

public class TestClassTest {

    private final TestClass testClass = new TestClass();

    @Test(expected = IllegalStateException.class)
    public void should_fail() {
        testClass.tryToTraverseStreamMultipleTimes(Arrays.asList("hey","hi","hello"));
    }
}