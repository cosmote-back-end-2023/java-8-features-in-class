package com.learning_actors.java8.stream.example22_parallel;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_calculate_cost_in_single_thread() {
        Customer customer = new Customer("Alexander", generatePhoneBills(20_000_000));

        long startTime = System.nanoTime();
        BigDecimal totalCost = phoneBillService.calculateTotalCost(customer);
        long endTime = System.nanoTime();

        long executionTimeInNanosec = endTime - startTime;
        System.out.println("Single Thread Execution time: " + TimeUnit.SECONDS.convert(executionTimeInNanosec, TimeUnit.NANOSECONDS));
    }

    @Test
    public void should_calculate_cost_in_parallel() {
        Customer customer = new Customer("Alexander", generatePhoneBills(20_000_000));

        long startTime = System.nanoTime();
        BigDecimal totalCost = phoneBillService.calculateTotalCostInParallel(customer);
        long endTime = System.nanoTime();

        long executionTimeInNanosec = endTime - startTime;
        System.out.println("Parallel Execution time: " + TimeUnit.SECONDS.convert(executionTimeInNanosec, TimeUnit.NANOSECONDS));
    }

    private List<PhoneBill> generatePhoneBills(int number) {
        return IntStream.range(0, number)
                .mapToObj(n -> new PhoneBill("2101234567", BigDecimal.valueOf(100)))
                .collect(Collectors.toList());
    }

}