package com.learning_actors.java8.stream.example01_intro;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StreamBasedPhoneBillServiceTest {

    private final StreamBasedPhoneBillService service = new StreamBasedPhoneBillService();

    @Test
    public void should_find_negative_phone_bills() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(-10)),
                new PhoneBill("3101234567", BigDecimal.valueOf(-30)),
                new PhoneBill("4101234567", BigDecimal.valueOf(0)),
                new PhoneBill("5101234567", BigDecimal.valueOf(10))
        ));

        List<PhoneBill> phoneBillsWithNegativeCost = service.findAllPhoneBillsWithNegativeCostOrdered(customer);

        assertEquals(phoneBillsWithNegativeCost.size(), 2);

        assertEquals("3101234567", phoneBillsWithNegativeCost.get(0).getPhoneNumber());
        assertEquals(BigDecimal.valueOf(-30), phoneBillsWithNegativeCost.get(0).getCost());

        assertEquals("2101234567", phoneBillsWithNegativeCost.get(1).getPhoneNumber());
        assertEquals(BigDecimal.valueOf(-10), phoneBillsWithNegativeCost.get(1).getCost());
    }

    @Test
    public void should_handle_not_available_phone_bills() {
        Customer customer = new Customer("Alexander", Collections.emptyList());

        List<PhoneBill> phoneBillsWithNegativeCost = service.findAllPhoneBillsWithNegativeCostOrdered(customer);

        assertTrue(phoneBillsWithNegativeCost.isEmpty());
    }

}
