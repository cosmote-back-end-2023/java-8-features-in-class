package com.learning_actors.java8.stream.example07_skip;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CsvServiceTest {

    private final CsvService csvService = new CsvService();

    @Test
    public void should_return_the_csv_content_withou_the_header() {
        List<String> csvContent = Arrays.asList(
                "customer,phoneNumber,cost",
                "Alexander,2101234567,200",
                "Katerina,2111234567,300"
        );

        List<String> csvData = csvService.getCsvContentWithoutTheHeader(csvContent);

        assertEquals(Arrays.asList("Alexander,2101234567,200", "Katerina,2111234567,300"), csvData);
    }


}