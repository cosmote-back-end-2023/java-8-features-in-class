package com.learning_actors.java8.stream.example15_to_set_map_collection;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_return_all_phone_numbers_as_set() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(-40)),
                new PhoneBill("4101234567", BigDecimal.valueOf(-10))
        ));

        Set<String> phoneNumbers = phoneBillService.getAllPhoneNumbersAsSet(customer);

        assertEquals(3, phoneNumbers.size());
    }

    @Test
    public void should_return_all_phone_numbers_as_linkedlist() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(-40))
        ));

        LinkedList<String> phoneNumbers = phoneBillService.getAllPhoneNumbersAsLinkedList(customer);

        assertEquals(3, phoneNumbers.size());
    }

    @Test
    public void should_return_all_phone_numbers_as_map() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        Map<String, PhoneBill> phoneBillsByNumber = phoneBillService.getAllPhoneBillsByPhoneNumber(customer);

        assertEquals(3, phoneBillsByNumber.size());

        assertEquals("2101234567", phoneBillsByNumber.get("2101234567").getPhoneNumber());
        assertEquals("3101234567", phoneBillsByNumber.get("3101234567").getPhoneNumber());
        assertEquals("4101234567", phoneBillsByNumber.get("4101234567").getPhoneNumber());
    }

}