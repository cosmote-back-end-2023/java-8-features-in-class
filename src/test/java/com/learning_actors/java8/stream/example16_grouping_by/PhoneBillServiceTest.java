package com.learning_actors.java8.stream.example16_grouping_by;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_group_bills_by_phone_number() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("2101234567", BigDecimal.valueOf(500)),
                new PhoneBill("3101234567", BigDecimal.valueOf(-40))
        ));

        Map<String, List<PhoneBill>> billsGroupedByPhoneNumber = phoneBillService.getPhoneBillsGroupingByPhoneNumber(customer);

        assertEquals(billsGroupedByPhoneNumber.size(), 2);

        assertEquals(2, billsGroupedByPhoneNumber.get("2101234567").size());
        assertEquals(1 ,billsGroupedByPhoneNumber.get("3101234567").size());
    }

    @Test
    public void should_calculate_number_of_bills_by_phone_number() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("2101234567", BigDecimal.valueOf(500)),
                new PhoneBill("3101234567", BigDecimal.valueOf(-40))
        ));

        Map<String, Long> billsGroupedByPhoneNumber = phoneBillService.getNumberOfBillsGroupingByPhoneNumber(customer);

        assertEquals(billsGroupedByPhoneNumber.size(), 2);

        assertEquals(new Long(2), billsGroupedByPhoneNumber.get("2101234567"));
        assertEquals(new Long(1), billsGroupedByPhoneNumber.get("3101234567"));
    }

    @Test
    public void should_calculate_total_cost_of_bills_by_phone_number() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("2101234567", BigDecimal.valueOf(500)),
                new PhoneBill("3101234567", BigDecimal.valueOf(40))
        ));

        Map<String, BigDecimal> billsGroupedByPhoneNumber = phoneBillService.getTotalCostOfBillsGroupingByPhoneNumber(customer);

        assertEquals(billsGroupedByPhoneNumber.size(), 2);

        assertEquals(0, BigDecimal.valueOf(600).compareTo(billsGroupedByPhoneNumber.get("2101234567")));
        assertEquals(0, BigDecimal.valueOf(40).compareTo(billsGroupedByPhoneNumber.get("3101234567")));
    }

}