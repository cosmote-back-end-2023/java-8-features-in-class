package com.learning_actors.java8.stream.example10_foreach;

import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_print_all_phone_numbers() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(-100)),
                new PhoneBill("2111234567", BigDecimal.valueOf(250)),
                new PhoneBill("2121234567", BigDecimal.valueOf(-150)),
                new PhoneBill("2131234567", BigDecimal.valueOf(200))
        ));

        phoneBillService.printAllPhoneNumbers(customer);

        //Nothing to check, have a look the console output
    }

}