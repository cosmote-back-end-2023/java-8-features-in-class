package com.learning_actors.java8.stream.example03_map;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_return_all_phone_numbers() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("2111234567", BigDecimal.valueOf(200)),
                new PhoneBill("2121234567", BigDecimal.valueOf(201))
        ));

        List<String> phoneNumbers = phoneBillService.getAllPhoneNumbers(customer);

        assertEquals(Arrays.asList("2101234567", "2111234567", "2121234567"), phoneNumbers);
    }

}