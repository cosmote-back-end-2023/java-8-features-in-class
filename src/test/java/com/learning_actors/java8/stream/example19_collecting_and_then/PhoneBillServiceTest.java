package com.learning_actors.java8.stream.example19_collecting_and_then;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test(expected = UnsupportedOperationException.class)
    public void should_retrieve_all_phone_numbers_in_an_unmodifiable_list() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(-40))
        ));

        List<String> phoneNumbers = phoneBillService.getPhoneNumbersUnmodifiableList(customer);

        assertEquals(3, phoneNumbers.size());

        phoneNumbers.add("1");//test that the list cannot be modified
    }

}