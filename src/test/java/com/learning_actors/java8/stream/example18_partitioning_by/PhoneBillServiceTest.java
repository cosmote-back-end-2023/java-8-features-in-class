package com.learning_actors.java8.stream.example18_partitioning_by;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_retrieve_all_phone_bills_partitioned_by_negative_amount() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(-40))
        ));

        Map<Boolean, List<PhoneBill>> billsPartitionedByNegativeCost = phoneBillService.getPhoneBillsPartitionedByNegativeCost(customer);

        assertEquals(2, billsPartitionedByNegativeCost.size());

        assertEquals(1, billsPartitionedByNegativeCost.get(true).size());
        assertEquals(2, billsPartitionedByNegativeCost.get(false).size());
    }

}