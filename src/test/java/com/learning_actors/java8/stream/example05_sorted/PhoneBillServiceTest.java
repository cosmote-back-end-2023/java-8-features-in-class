package com.learning_actors.java8.stream.example05_sorted;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_return_all_phone_bills_sorted_by_cost() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("2111234567", BigDecimal.valueOf(250)),
                new PhoneBill("2121234567", BigDecimal.valueOf(150)),
                new PhoneBill("2131234567", BigDecimal.valueOf(200))
        ));

        List<PhoneBill> phoneBillsSorted = phoneBillService.getAllPhoneBillsSortedByCost(customer);

        assertEquals(phoneBillsSorted.size(), 4);

        assertEquals("2101234567", phoneBillsSorted.get(0).getPhoneNumber());
        assertEquals("2121234567", phoneBillsSorted.get(1).getPhoneNumber());
        assertEquals("2131234567", phoneBillsSorted.get(2).getPhoneNumber());
        assertEquals("2111234567", phoneBillsSorted.get(3).getPhoneNumber());
    }

}