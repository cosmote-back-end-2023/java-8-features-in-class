package com.learning_actors.java8.stream.example06_limit;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_return_3_phone_bills() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("2111234567", BigDecimal.valueOf(250)),
                new PhoneBill("2121234567", BigDecimal.valueOf(150)),
                new PhoneBill("2131234567", BigDecimal.valueOf(200))
        ));

        List<PhoneBill> phoneBills = phoneBillService.get3PhoneBills(customer);

        assertEquals(3, phoneBills.size());
    }

    @Test
    public void should_return_less_than_3_phone_bills_if_there_are_no_more_than_3() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("2111234567", BigDecimal.valueOf(250))
        ));

        List<PhoneBill> phoneBills = phoneBillService.get3PhoneBills(customer);

        assertEquals(2, phoneBills.size());
    }

}