package com.learning_actors.java8.stream.example10_reduce;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService service = new PhoneBillService();

    @Test
    public void should_calculate_total_cost() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(-10)),
                new PhoneBill("3101234567", BigDecimal.valueOf(5)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        BigDecimal totalCost = service.calculateTotalCost(customer);

        assertEquals(BigDecimal.valueOf(35), totalCost);
    }

    @Test
    public void should_calculate_zero_bills() {
        Customer customer = new Customer("Alexander", Collections.emptyList());

        BigDecimal totalCost = service.calculateTotalCost(customer);

        assertEquals(BigDecimal.ZERO, totalCost);
    }

}