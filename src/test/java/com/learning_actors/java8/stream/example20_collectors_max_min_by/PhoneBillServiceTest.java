package com.learning_actors.java8.stream.example20_collectors_max_min_by;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PhoneBillServiceTest {

    PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_find_most_expensive_bill() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        Optional<PhoneBill> mostExpensivePhoneBill = phoneBillService.getMostExpensivePhoneBill(customer);

        assertTrue(mostExpensivePhoneBill.isPresent());

        assertEquals("3101234567", mostExpensivePhoneBill.get().getPhoneNumber());
    }

    @Test
    public void should_find_most_expensive_bill_per_phone_number() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("3101234567", BigDecimal.valueOf(40))
        ));

        Map<String, PhoneBill> mostExpensiveBillPerPhoneNumber = phoneBillService.getMostExpensivePhoneBillPerPhoneNumber(customer);

        assertEquals(2, mostExpensiveBillPerPhoneNumber.size());

        assertEquals(0, BigDecimal.valueOf(100).compareTo(mostExpensiveBillPerPhoneNumber.get("2101234567").getCost()));
        assertEquals(0, BigDecimal.valueOf(500).compareTo(mostExpensiveBillPerPhoneNumber.get("3101234567").getCost()));
    }

}