package com.learning_actors.java8.stream.example17_joining;

import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_return_phone_number_as_a_string_separated_with_comma() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(-40))
        ));

        String phoneNumbers = phoneBillService.getAllPhoneNumberWithComma(customer);

        assertEquals("2101234567,3101234567,4101234567", phoneNumbers);
    }

}