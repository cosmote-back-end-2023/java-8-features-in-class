package com.learning_actors.java8.stream.example02_filter;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_return_all_phone_bills_that_exceed_the_max_cost_limit() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("2111234567", BigDecimal.valueOf(200)),
                new PhoneBill("2121234567", BigDecimal.valueOf(201)),
                new PhoneBill("2131234567", BigDecimal.valueOf(202))
                )
        );

        List<PhoneBill> phoneBillsOverLimit = phoneBillService.getPhoneBillsThatExceedCostLimit(customer);

        assertEquals(phoneBillsOverLimit.size(), 2);
        assertEquals("2121234567", phoneBillsOverLimit.get(0).getPhoneNumber());
        assertEquals("2131234567", phoneBillsOverLimit.get(1).getPhoneNumber());
    }

}