package com.learning_actors.java8.stream.example21_collectors_mapping;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_return_all_phone_numbers() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        List<String> phoneNumbers = phoneBillService.getPhoneNumbers(customer);

        assertEquals(Arrays.asList("2101234567", "3101234567", "4101234567"), phoneNumbers);
    }

    @Test
    public void should_return_bill_costs_per_phone_number() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("3101234567", BigDecimal.valueOf(40))
        ));

        Map<String, List<BigDecimal>> billCostsPerPhoneNumbers = phoneBillService.getBillCostsPerPhoneNumbers(customer);

        assertEquals(2, billCostsPerPhoneNumbers.size());

        assertEquals(1, billCostsPerPhoneNumbers.get("2101234567").size());//100
        assertEquals(2, billCostsPerPhoneNumbers.get("3101234567").size());//500,40
    }

}