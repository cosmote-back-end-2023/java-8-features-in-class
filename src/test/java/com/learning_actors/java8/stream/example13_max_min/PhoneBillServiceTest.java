package com.learning_actors.java8.stream.example13_max_min;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_find_most_expensive_bill() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        Optional<PhoneBill> mostExpensivePhoneBill = phoneBillService.findMostExpensivePhoneBill(customer);

        assertTrue(mostExpensivePhoneBill.isPresent());
        assertEquals("3101234567", mostExpensivePhoneBill.get().getPhoneNumber());
    }

    @Test
    public void should_find_less_expensive_bill() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        Optional<PhoneBill> lessExpensivePhoneBill = phoneBillService.findLessExpensivePhoneBill(customer);

        assertTrue(lessExpensivePhoneBill.isPresent());
        assertEquals("4101234567", lessExpensivePhoneBill.get().getPhoneNumber());
    }

    @Test
    public void should_handle_case_of_customer_without_bills() {
        Customer customer = new Customer("Alexander", Collections.emptyList());

        Optional<PhoneBill> mostExpensivePhoneBill = phoneBillService.findMostExpensivePhoneBill(customer);
        Optional<PhoneBill> lessExpensivePhoneBill = phoneBillService.findLessExpensivePhoneBill(customer);

        assertFalse(mostExpensivePhoneBill.isPresent());
        assertFalse(lessExpensivePhoneBill.isPresent());
    }

}