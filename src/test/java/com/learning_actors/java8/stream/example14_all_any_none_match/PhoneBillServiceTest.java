package com.learning_actors.java8.stream.example14_all_any_none_match;

import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_check_if_all_bills_are_negative_true_scenario() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(-100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(-500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(-40))
        ));

        boolean areAllPhoneBillsNegative = phoneBillService.areAllPhoneBillsNegative(customer);

        assertTrue(areAllPhoneBillsNegative);
    }

    @Test
    public void should_check_if_all_bills_are_negative_false_scenario() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(-100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(-500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        boolean areAllPhoneBillsNegative = phoneBillService.areAllPhoneBillsNegative(customer);

        assertFalse(areAllPhoneBillsNegative);
    }

    @Test
    public void should_check_if_any_bill_are_negative_true_scenario() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(-40))
        ));

        boolean isAnyPhoneBillNegative = phoneBillService.isAnyPhoneBillNegative(customer);

        assertTrue(isAnyPhoneBillNegative);
    }

    @Test
    public void should_check_if_any_bill_is_negative_false_scenario() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        boolean isAnyPhoneBillNegative = phoneBillService.isAnyPhoneBillNegative(customer);

        assertFalse(isAnyPhoneBillNegative);
    }

    @Test
    public void should_check_if_all_bills_are_not_negative_true_scenario() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(40))
        ));

        boolean areAllPhoneBillsNotNegative = phoneBillService.areAllPhoneBillsNotNegative(customer);

        assertTrue(areAllPhoneBillsNotNegative);
    }

    @Test
    public void should_check_if_all_bills_are_not_negative_false_scenario() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(100)),
                new PhoneBill("3101234567", BigDecimal.valueOf(500)),
                new PhoneBill("4101234567", BigDecimal.valueOf(-40))
        ));

        boolean areAllPhoneBillsNotNegative = phoneBillService.areAllPhoneBillsNotNegative(customer);

        assertFalse(areAllPhoneBillsNotNegative);
    }

}