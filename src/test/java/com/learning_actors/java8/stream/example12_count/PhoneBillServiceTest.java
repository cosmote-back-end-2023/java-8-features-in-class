package com.learning_actors.java8.stream.example12_count;

import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.Test;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;

import static org.junit.Assert.assertEquals;

public class PhoneBillServiceTest {

    private final PhoneBillService phoneBillService = new PhoneBillService();

    @Test
    public void should_retrieve_the_number_of_negative_bills() {
        Customer customer = new Customer("Alexander", Arrays.asList(
                new PhoneBill("2101234567", BigDecimal.valueOf(-100)),
                new PhoneBill("2111234567", BigDecimal.valueOf(250)),
                new PhoneBill("2121234567", BigDecimal.valueOf(-150)),
                new PhoneBill("2131234567", BigDecimal.valueOf(200))
        ));

        long countOfNegativeBills = phoneBillService.getNumberOfNegativePhoneBills(customer);

        assertEquals(2, countOfNegativeBills);
    }

}