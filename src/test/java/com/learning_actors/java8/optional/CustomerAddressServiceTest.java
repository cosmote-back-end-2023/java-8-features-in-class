package com.learning_actors.java8.optional;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Test;

import com.learning_actors.java8.common.Address;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;
import com.learning_actors.java8.common.Street;

public class CustomerAddressServiceTest {

    private final CustomerAddressService service = new CustomerAddressServiceImpl();

    @Test(expected = NullPointerException.class)
    public void should_fail_for_no_address_available() {
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))
        ));

        service.getCustomerPostalCode(customer);

    }


    @Test
    public void should_not_fail_for_street_name_for_no_address_provided() {
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))
        ));

        String result = service.getCustomerStreetName(customer);

        assertEquals("No street name found for this customer", result);

    }

    @Test
    public void should_return_customer_street_name() {
        Street street = new Street("Alexandras", "16");
        Address address = new Address("11525", "Athens", "Attica", street);
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))),
        address);

        String streetName = service.getCustomerStreetName(customer);

        assertEquals("Alexandras", streetName);

    }

}
