package com.learning_actors.java8.optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Test;

import com.learning_actors.java8.common.Address;
import com.learning_actors.java8.common.Customer;
import com.learning_actors.java8.common.PhoneBill;
import com.learning_actors.java8.common.Street;

public class CustomerAddressServiceOptionalTest {

    private final CustomerAddressService service = new CustomerAddressServiceOptionalImpl();

    @Test
    public void should_return_customer_street_name() {
        Street street = new Street("Alexandras", "16");
        Address address = new Address("11525", "Athens", "Attica", street);
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))),
            address);

        String streetName = service.getCustomerStreetName(customer);

        assertEquals("Alexandras", streetName);

    }

    @Test
    public void should_return_customer_postal_code() {
        Street street = new Street("Alexandras", "16");
        Address address = new Address("11525", "Athens", "Attica", street);
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))),
            address);

        String customerPostalCode = service.getCustomerPostalCode(customer);

        assertEquals("11525", customerPostalCode);

    }

    @Test
    public void should_return_customer_appartment_letter() {
        Street street = new Street("Alexandras", "16", Optional.of("A"));
        Address address = new Address("11525", "Athens", "Attica", street);
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))),
            address);

        String customerApartmentNumber = ((CustomerAddressServiceOptionalImpl) service).getCustomerAppartmentLetter(customer);

        assertEquals("A", customerApartmentNumber);

    }

    @Test
    public void should_return_customer_optional_appartment_letter() {
        Street street = new Street("Alexandras", "16", Optional.of("A"));
        Address address = new Address("11525", "Athens", "Attica", street);
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))),
            address);

        Optional<String> customerApartmentNumber = ((CustomerAddressServiceOptionalImpl) service).getOptionalCustomerAppartmentLetter(customer);

        assertEquals("A", customerApartmentNumber.get());

    }


    @Test
    public void should_not_fail_for_postal_code_for_no_address_provided() {
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))
        ));

        String result = service.getCustomerPostalCode(customer);

        assertEquals("No postal code registered for this customer", result);

    }

    @Test
    public void should_not_fail_for_street_name_for_no_address_provided() {
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))
        ));

        String result = service.getCustomerStreetName(customer);

        assertEquals("No street name found for this customer", result);

    }

    @Test
    public void should_not_fail_for_appartment_number_for_no_address_provided() {
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))
        ));

        String result = ((CustomerAddressServiceOptionalImpl) service).getCustomerAppartmentLetter(customer);

        assertEquals("No appartment letter for this address", result);

    }

    @Test
    public void should_not_fail_for_optional_appartment_number_for_no_address_provided() {
        Customer customer = new Customer("Alexander", Arrays.asList(
            new PhoneBill("2101234567", BigDecimal.valueOf(-1)),
            new PhoneBill("3101234567", BigDecimal.valueOf(0)),
            new PhoneBill("4101234567", BigDecimal.valueOf(1))
        ));

        Optional<String> result = ((CustomerAddressServiceOptionalImpl) service).getOptionalCustomerAppartmentLetter(customer);

        assertFalse(result.isPresent());

    }



}
